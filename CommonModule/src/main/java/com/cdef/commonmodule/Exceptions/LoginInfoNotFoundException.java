package com.cdef.commonmodule.Exceptions;

/**
 * Created by kimjinmo on 2018. 7. 3..
 */

public class LoginInfoNotFoundException extends Exception {

    public LoginInfoNotFoundException(String message) {
        super(message);
    }
}
