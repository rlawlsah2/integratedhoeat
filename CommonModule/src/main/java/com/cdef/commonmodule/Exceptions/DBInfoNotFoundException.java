package com.cdef.commonmodule.Exceptions;

/**
 * Created by kimjinmo on 2018. 7. 3..
 */

public class DBInfoNotFoundException extends Exception {

    public DBInfoNotFoundException(String message) {
        super(message);
    }
}
