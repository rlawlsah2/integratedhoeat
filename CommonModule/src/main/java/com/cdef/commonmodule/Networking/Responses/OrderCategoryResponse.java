package com.cdef.commonmodule.Networking.Responses;

import com.cdef.commonmodule.DataModel.OrderCategoryData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kimjinmo on 2016. 11. 10..
 */

public class OrderCategoryResponse extends BaseResponse {


    @SerializedName("result")
    @Expose
    public OrderCategoryData result = new OrderCategoryData();
}
