package com.cdef.commonmodule.Networking;

import android.support.annotation.Keep;

import com.cdef.commonmodule.DataModel.TableMapInfoData;
import com.cdef.commonmodule.Networking.Responses.ADActionResponse;
import com.cdef.commonmodule.Networking.Responses.ADResponse;
import com.cdef.commonmodule.Networking.Responses.LoginResponse;
import com.cdef.commonmodule.Networking.Responses.MenuResponse;
import com.cdef.commonmodule.Networking.Responses.NoticeResponse;
import com.cdef.commonmodule.Networking.Responses.OrderCategoryResponse;
import com.cdef.commonmodule.Networking.Responses.OrderResponse;
import com.cdef.commonmodule.Networking.Responses.RandomADResponse;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by ennur on 6/25/16.
 */
public interface NetworkService {
    @Keep
    @POST
    Observable<LoginResponse> Login(@Url String url,
                                    @Query("branchId") String branchId,
                                    @Query("password") String password,
                                    @Query("tableNo") String tableNo
    );



    @Keep
    @POST("admin/api/login?isStaffLogin=true")
    Observable<LoginResponse> LoginForManager(
            @Body RequestBody array
    );


    @Keep
    @GET
    Observable<OrderCategoryResponse> getOrderMainCategoryList(@Url String url
    );

    @Keep
    @POST
    Observable<NoticeResponse> getNoticeList(@Url String url
    );



    @Keep
    @POST
    Observable<OrderResponse> order(
            @Url String url,
            @Body RequestBody array
    );



    @Keep
    @GET
    Observable<MenuResponse> getMenu(@Url String url
    );

    @Keep
    @GET
    Observable<ADResponse> getBannerList(@Url String url
    );

    @Keep
    @GET
    Observable<RandomADResponse> getRandomBanner(@Url String url
    );

    @Keep
    @GET
    Observable<RandomADResponse> getBannerDetail(@Url String url
    );

    /**
     * 광고 참여에 대한 결과를 가져오는 역할
     **/
    @Keep
    @GET
    Observable<ADActionResponse> getADAction(@Url String url
    );
    @Keep
    @GET
    Observable<ArrayList<TableMapInfoData>> getMapInfo(@Url String url
    );


    @Keep
    @GET
    Observable<MenuResponse> getMenuFlagship(@Url String url
    );

}
