package com.cdef.commonmodule.Networking;


import android.util.Log;

import com.cdef.commonmodule.BuildConfig;
import com.cdef.commonmodule.DataModel.TableMapInfoData;
import com.cdef.commonmodule.Networking.Responses.OrderResponse;
import com.cdef.commonmodule.Utils.ErrorUtils;
import com.cdef.commonmodule.Networking.Responses.ADActionResponse;
import com.cdef.commonmodule.Networking.Responses.ADResponse;
import com.cdef.commonmodule.Networking.Responses.BaseResponse;
import com.cdef.commonmodule.Networking.Responses.LoginResponse;
import com.cdef.commonmodule.Networking.Responses.MenuResponse;
import com.cdef.commonmodule.Networking.Responses.NoticeResponse;
import com.cdef.commonmodule.Networking.Responses.OrderCategoryResponse;
import com.cdef.commonmodule.Networking.Responses.RandomADResponse;
import com.cdef.commonmodule.Utils.LogUtil;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;

/**
 * Created by ennur on 6/25/16.
 */
public class Service {
    private final NetworkService networkService;

    public NetworkService getNetworkService() {
        return this.networkService;
    }

    public Service(NetworkService networkService) {
        this.networkService = networkService;
    }

    /**
     * 로그인 시도
     **/
//    public Subscription Login(String branchId, String password, String tableNo, GetRequestCallBack callBack) {
//        return this.networkService.Login(
//                BuildConfig.LoginURL + "admin/api/login",
//                branchId,
//                password,
//                tableNo
//        )
//                .subscribeOn(Schedulers.computation())
//                .observeOn(AndroidSchedulers.mainThread())
//                .onErrorResumeNext(throwable -> Observable.error(throwable))
//                .subscribe(
//                        Response -> requestErrorHandler(Response),
//                        error -> callBack.onError(new NetworkError(error)),
//                        () -> Log.d("kk9991", "request complete")
//
//                );
//    }


    ////점원용 로그인
    public Observable<LoginResponse> loginForManager(String branchId, String password) {

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("branchId", branchId)
                .addFormDataPart("password", password)
                .build();

        return this.networkService.LoginForManager(
                requestBody
        );

    }

    ////renew 로그인
    public Observable<LoginResponse> getLoginInfo(String branchId, String password, String tableNo) {
        return this.networkService.Login(
                BuildConfig.LoginURL + "admin/api/login",
                branchId,
                password,
                tableNo
        );
    }

    /**
     * 메뉴 카테고리 불러오기
     **/
    public Observable<OrderCategoryResponse> getMainCategoryList() {
        return this.networkService.getOrderMainCategoryList(
                BuildConfig.LoginURL + "admin/api/categories"
        );
    }


    /**
     * 메뉴 서브 카테고리 불러오기
     **/
    public Observable<OrderCategoryResponse> getSubCategoryList(String categoryId) {
        return this.networkService.getOrderMainCategoryList(
                BuildConfig.LoginURL + "admin/api/categories/" + categoryId
        );
    }


    /**
     * 공지사항 가져오기
     **/
    public Observable<NoticeResponse> getNoticeList(String branchUid) {
        return this.networkService.getNoticeList(
                BuildConfig.LoginURL + "api/store/notice?storeBranchUid=" + branchUid
        );

    }


    ////주문용 api
    public Observable<OrderResponse> order(String url, String orderJson) {


        LogUtil.d("주문 넣기 직전 : " + orderJson);


        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), orderJson);

        return this.networkService.order(url, requestBody);

    }

    /**
     * 메뉴 불러오기
     **/
    public Observable<MenuResponse> getMenus(String categoryId, String storeBranchUid) {
        return this.networkService.getMenu(
                BuildConfig.LoginURL + "admin/api/categories/" + categoryId + "/menus?storeBranchUid=" + storeBranchUid
        );
    }

    /**
     * 메뉴 불러오기 파라미터는 `flagship` : `{recommend, hot, new, present, game}`
     **/
    public Observable<MenuResponse> getMenuFlagship(String flagship, String storeBranchUid) {
        return this.networkService.getMenuFlagship(
                BuildConfig.LoginURL + "admin/api/menus/flagship?flagship=" + flagship + "&storeBranchUid=" + storeBranchUid
        );
    }

    /**
     * 배너 목록 불러오기
     *
     * @param members 인원수. 디폴트 0
     * @param sex     테이블 구성원 표시. male|female|both. 디폴트 both
     **/
    public Observable<ADResponse> getBannerList(int members, String sex) {
        StringBuilder sb = new StringBuilder();
        int paramCount = 0;
        sb.append(BuildConfig.LoginURL)
                .append("admin/api/banners?");


        sb.append("members=")
                .append(members);
        sb.append("&");
        sb.append("sex=")
                .append(sex);


        return this.networkService.getBannerList(
                sb.toString()
        );
    }

    /**
     * 랜덤으로 배너 광고 하나 가져오기
     *
     * @param members 인원수. 디폴트 0
     * @param sex     테이블 구성원 표시. male|female|both. 디폴트 both
     * @param section 광고의 유형. event | screen | launcher
     **/
    public Observable<RandomADResponse> getRandomBanner(String tableNo, int members, String sex, String section) {
        StringBuilder sb = new StringBuilder();
        int paramCount = 0;
        sb.append(BuildConfig.LoginURL)
                .append("admin/api/ad?");

        sb.append("members=")
                .append(members);
        sb.append("&sex=")
                .append(sex);
        sb.append("&section=")
                .append(section);
        sb.append("&tableNo=")
                .append(tableNo);
        return this.networkService.getRandomBanner(
                sb.toString()
        );
    }

    /**
     * 단일 배너를 조회할때 호출하는 API
     *
     * @param uuid    광고UUID
     * @param tableNo 테이블번호
     * @param members 인원수. 디폴트 0
     * @param sex     테이블 구성원 표시. male|female|both. 디폴트 na
     **/
    public Observable<RandomADResponse> getBannerDetail(String uuid, String tableNo, int members, String sex) {
        StringBuilder sb = new StringBuilder();
        int paramCount = 0;
        sb.append(BuildConfig.LoginURL)
                .append("admin/api/ad/")
                .append(uuid)
                .append("?");
        sb.append("members=")
                .append(members);
        sb.append("&sex=")
                .append(sex);
        sb.append("&tableNo=")
                .append(tableNo);

        return this.networkService.getBannerDetail(
                sb.toString()
        );
    }

    /***
     * 이벤트 참여하는 url
     * **/
    public Observable<ADActionResponse> getADAction(String uuid, String actionParam, String input, String tableNo, int members, String sex) {
        if (uuid == null) {
            return null;
        }
        int paramCount = 0;
        StringBuilder sb = new StringBuilder();
        sb.append(BuildConfig.LoginURL)
                .append("admin/api/ad/");
        sb.append(uuid).append("/action?");

        sb.append(actionParam)
                .append("=")
                .append(input);
        sb.append("&");
        sb.append("members=")
                .append(members);
        sb.append("&");
        sb.append("sex=")
                .append(sex);
        sb.append("&");
        sb.append("tableNo=")
                .append(tableNo);

        return this.networkService.getADAction(sb.toString());
    }


    /**
     * 맵 정보 가져오기
     **/
    public Observable<ArrayList<TableMapInfoData>> getMapInfo(String branchUid) {

        return this.networkService.getMapInfo(
                BuildConfig.LoginURL + "admin/api/branch/" + branchUid + "/tableMap"
        )
                ;
    }

    public interface GetRequestCallBack<T> {
        void onSuccess(T Response);

        void onError(NetworkError networkError);

        void onRequestError(String errorCode);
    }


    public static boolean requestErrorHandler(BaseResponse Response) {
        if (Response != null) {
            return ErrorUtils.isSuccess(Response.getStatus().code);
        } else
            return false;
    }

    private void requestErrorHandlerFORInternetCheck(BaseResponse Response, GetRequestCallBack callBack) {
        if (Response != null) {
            if (ErrorUtils.isSuccessForInternetCheck(Response.getStatus().code)) {
                Log.d("kk9991", "API 요청 성공 : " + Response.getStatus().code);
                callBack.onSuccess(Response);
            } else {
                Log.d("kk9991", "API 요청 실패");
                callBack.onRequestError(Response.getStatus().code);
            }

        }
    }


}
