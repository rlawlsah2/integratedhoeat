package com.cdef.commonmodule.Networking.Responses;

import com.cdef.commonmodule.DataModel.NoticeData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kimjinmo on 2016. 11. 10..
 */

public class NoticeResponse extends BaseResponse {


    @SerializedName("result")
    @Expose
    public NoticeData result = new NoticeData();
}
