package com.cdef.commonmodule.DataModel.FirebaseDataSet;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

@IgnoreExtraProperties
public class Present extends BaseData{

    public String time;
    public String menuName;
    public String menuImage;
    public String message;
    public boolean read;
    public String fromTableNo;
    public String key;

    public Present()
    {

    }

}

