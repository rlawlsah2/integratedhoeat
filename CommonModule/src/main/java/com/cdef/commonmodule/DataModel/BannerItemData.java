package com.cdef.commonmodule.DataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

/**
 * Created by kimjinmo on 2016. 11. 10..
 * <p>
 * 배너 목록
 */
@Generated("org.jsonschema2pojo")
public class BannerItemData extends BannerDefaultData {

    @SerializedName("adInfo")
    @Expose
    public BannerDefaultData adInfo = new BannerDefaultData();

}
