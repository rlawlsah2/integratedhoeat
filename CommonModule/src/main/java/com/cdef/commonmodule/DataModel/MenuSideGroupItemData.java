package com.cdef.commonmodule.DataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import javax.annotation.Generated;

/**
 * Created by kimjinmo on 2016. 11. 10..
 */
@Generated("org.jsonschema2pojo")
public class MenuSideGroupItemData extends BaseData {

    public int sideGroupUid;
    public int storeUid;
    public String groupName;
    public int isNecessary; ///1이면 필수 아니면
    public int maxGroupItem; ///1개 이상 선택

    @SerializedName("sideOptions")
    @Expose
    public ArrayList<MenuSideOptionItemData> sideOptions = new ArrayList<>();
}
