package com.cdef.commonmodule.DataModel;

/**
 * Created by kimjinmo on 2018. 3. 28..
 */


public class ADActionData extends BaseData {

    public String actionResult;
    public Reward reward;

    public class Reward extends BaseData {

        public String rewardType;
        public String rewardCode;
        public int value;
    }


}