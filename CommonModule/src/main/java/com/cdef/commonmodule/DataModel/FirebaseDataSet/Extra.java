package com.cdef.commonmodule.DataModel.FirebaseDataSet;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

@IgnoreExtraProperties

    public class Extra extends BaseData
    {
        public String ref;
        public String reference;
        public String reason;
        public String orderId;
        public Extra()
        {

        }
}

