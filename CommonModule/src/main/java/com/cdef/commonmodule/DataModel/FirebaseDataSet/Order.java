package com.cdef.commonmodule.DataModel.FirebaseDataSet;

import android.support.annotation.Nullable;

import com.cdef.commonmodule.DataModel.MenuItemData;
import com.cdef.commonmodule.Utils.Utils;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

@IgnoreExtraProperties
public class Order extends BaseData{

    //기존
//
//    public String key;
//    public String foodName;
//    public String price;
//    public String quantity;
//    public String CMDTCD;
//    public String categoryName;
//    public String categoryCode;
//    public Long time;
//

    //개편
    public int foodUid;
    public String foodName;
    public int price;
    public int quantity;
    public int state = 1;       // (0 : 요리중 / 1: 대기중 / 2: 완료)
    public String time;
    public String categoryCode;
    public String categoryName;
    public String CMDTCD;
    public String key;
    public int isKitchen;
    public String orderedTableNo;
    public String tableNo;
    public String read;
    public String orderKey;
    public String orderId;
    public String status;


    public Order()
    {

    }


    @Nullable
    @SerializedName("options")
    @Expose
    public HashMap<String, Order> options = new HashMap<>();


    public Order setData(MenuItemData mSelectedMenuItemData, int quantity) {
        this.CMDTCD = mSelectedMenuItemData.CMDTCD;
        this.categoryCode = mSelectedMenuItemData.tag;  ///tag가 상위 카테고리
        this.foodName = mSelectedMenuItemData.foodName;
        this.foodUid = mSelectedMenuItemData.foodUid;
        this.isKitchen = mSelectedMenuItemData.isKitchen;
        this.price = mSelectedMenuItemData.price;
        this.quantity = quantity;
        this.time = Utils.getDate("yy-MM-dd HH:mm:ss");
        return this;
    }
}

