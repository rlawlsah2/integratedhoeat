package com.cdef.commonmodule.DataModel;

import java.util.ArrayList;

/**
 * Created by kimjinmo on 2018. 3. 28..
 */


public class BannerDefaultData extends BaseData {

    public String uuid;
    public String bannerImage;
    public String target;
    public ArrayList<FileURL> files;
    public String adType;
    public String actionParam;
    public String actionParamType;


    public class FileURL extends BaseData {
        public String fileUrl;
    }

}