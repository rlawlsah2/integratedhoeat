package com.cdef.commonmodule.DataModel.FirebaseDataSet;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

@IgnoreExtraProperties
public class TableInfo {
    public String allChat_startAt;
    public String registrationToken;
    public String IFSA_ORDER_ID;
    public TableMember members;
    public String gameId;
    public String tableType;



    public TableInfo()
    {

    }



}

