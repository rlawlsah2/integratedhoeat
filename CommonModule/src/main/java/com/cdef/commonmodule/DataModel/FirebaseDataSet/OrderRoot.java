package com.cdef.commonmodule.DataModel.FirebaseDataSet;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.List;
import java.util.Map;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

@IgnoreExtraProperties
public class OrderRoot extends BaseData{


    public Map<String, List<Order>> orders;

    public OrderRoot()
    {

    }
}

