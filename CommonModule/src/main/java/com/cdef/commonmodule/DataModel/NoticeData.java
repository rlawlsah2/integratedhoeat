package com.cdef.commonmodule.DataModel;

import java.util.ArrayList;
import java.util.Random;

import javax.annotation.Generated;

/**
 * Created by kimjinmo on 2016. 11. 10..
 */
@Generated("org.jsonschema2pojo")
public class NoticeData extends BaseData {

    Random random = new Random();
    public int totalCount;
    public ArrayList<NoticeItemData> notice;

    public String getRandomNotice() {
        if (notice != null && notice.size() > 0) {
            return notice.get(random.nextInt(notice.size())).content;
        }
        return "";
    }

    public class NoticeItemData extends BaseData {

        public int contentUid = 0;
        public int boardId = 0;
        public int active = 0;
        public String title;
        public String content;
        public String regDate;
        public String branchName;

    }
}
