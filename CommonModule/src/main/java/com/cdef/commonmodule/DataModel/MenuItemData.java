package com.cdef.commonmodule.DataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import javax.annotation.Generated;

/**
 * Created by kimjinmo on 2016. 11. 10..
 */
@Generated("org.jsonschema2pojo")
public class MenuItemData extends BaseData {



    public int menuUid;
    public int order;
    public int foodUid;
    public String foodImage;
    public String foodName;
    public String description;
    public String tag;
    public String CMDTCD;
    public int price;
    public int newFlag;
    public int hotFlag;
    public int recommendFlag;
    public int useFlag;
    public int quantity;
    public int soldOut;
    public int isKitchen;
    public int defaultItem;
    public int maxQuantity;

    ///
    public int isSet;


    @SerializedName("sideGroup")
    @Expose
    public ArrayList<MenuSideGroupItemData> sideGroup = new ArrayList<>();

}
