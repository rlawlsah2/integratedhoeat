package com.cdef.commonmodule.DataModel;

import android.text.TextUtils;

import javax.annotation.Generated;

/**
 * Created by kimjinmo on 2016. 11. 10..
 */
@Generated("org.jsonschema2pojo")
public class TableMapInfoData extends BaseData {

    public int x;
    public int y;
    public int width;
    public int height;
    public String tNo;

    public TableMapInfoData(String tNo)
    {
        this.tNo = tNo;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TableMapInfoData) {
            TableMapInfoData another = (TableMapInfoData) obj;
            return TextUtils.equals(this.tNo, another.tNo);
        }

        return false;
    }
}
