package com.cdef.commonmodule.DataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import javax.annotation.Generated;

/**
 * Created by kimjinmo on 2016. 11. 10..
 *
 * 배너 목록
 */
@Generated("org.jsonschema2pojo")
public class BannerListData extends BannerDefaultData {


    @SerializedName("banners")
    @Expose
    public ArrayList<BannerDefaultData> banners = new ArrayList<>();

}
