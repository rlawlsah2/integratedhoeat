package com.cdef.commonmodule.Repository;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import com.cdef.commonmodule.Exceptions.LoginInfoNotFoundException;
import com.cdef.commonmodule.Utils.LogUtil;
import com.cdef.commonmodule.Utils.LoginUtil;
import com.cdef.commonmodule.Networking.Service;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class BaseRepository {

    int refreshTokenAttemptCount = 0;


    protected MutableLiveData<Service> service = new MutableLiveData<>();

    public BaseRepository(Service service) {
        this.service.setValue(service);
    }


    ///로그인 정보를 가져오기 위한 코드
    public Observable<Object> updateAccessToken(Context context, String branchId, String pw, String tableNo) {

        ///1. 로컬에 저장된 로그인 정보가 없는지 확인할것.

        ///2. 로컬에 없다면 서버에서 가져와야한다
        return this.service.getValue().getLoginInfo(branchId, pw, tableNo)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> Observable.error(throwable))
                .flatMap(loginResponse -> {
                    LogUtil.d("로그인 중 flatMap : " + (Service.requestErrorHandler(loginResponse)));
                    if (Service.requestErrorHandler(loginResponse)) {
                        LoginUtil.getInstance(context).updateLoginIfo(context,loginResponse.result.accessToken);
                        return Observable.just(true);
                    } else {
                        return Observable.error(new LoginInfoNotFoundException("cannot find loginInfo"));
                    }
                });
    }
}
