package com.cdef.commonmodule.Repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.cdef.commonmodule.DataModel.LoginData;
import com.cdef.commonmodule.DataModel.MenuItemData;
import com.cdef.commonmodule.DataModel.OrderCategoryData;
import com.cdef.commonmodule.DataModel.TableMapInfoData;
import com.cdef.commonmodule.Exceptions.LoginInfoNotFoundException;
import com.cdef.commonmodule.Utils.LogUtil;
import com.cdef.commonmodule.Utils.LoginUtil;
import com.cdef.commonmodule.Networking.Service;
import com.cdef.commonmodule.Utils.TabletSetting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class LoginRepository extends BaseRepository {

    public LinkedHashMap<String, Boolean> subCategories_b = new LinkedHashMap<String, Boolean>();
    public LinkedHashMap<String, ArrayList<MenuItemData>> allMenus = new LinkedHashMap<>();///코코이찌방야에서 사용중
    public LinkedHashMap<String, ArrayList<OrderCategoryData.CategoryItem>> subCategories = new LinkedHashMap<>();
    public ArrayList<OrderCategoryData.CategoryItem> mainCategories = new ArrayList<>();

    public LoginRepository(Service service) {
        super(service);
    }

    MutableLiveData<LoginData> loginInfo = new MutableLiveData<>();
    MutableLiveData<ArrayList<TableMapInfoData>> tableMapInfo = new MutableLiveData<>();

    public Observable<LiveData<LoginData>> getLoginInfo(String branchId, String pw, String tableNo) {

        ///1. 로컬에 저장된 로그인 정보가 없는지 확인할것.

        ///2. 로컬에 없다면 서버에서 가져와야한다
        return this.service.getValue().getLoginInfo(branchId, pw, tableNo)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> Observable.error(throwable))
                .flatMap(loginResponse -> {
                    LogUtil.d("로그인 중 flatMap : " + (Service.requestErrorHandler(loginResponse)));
                    if (Service.requestErrorHandler(loginResponse)) {
                        loginInfo.setValue(loginResponse.result);
                    } else {
                        return Observable.error(new NullPointerException("cannot login"));
                    }
                    return Observable.just(loginInfo);
                });
    }

    public Observable<LiveData<LoginData>> loginForManager(String branchId, String pw) {

        ///1. 로컬에 저장된 로그인 정보가 없는지 확인할것.

        ///2. 로컬에 없다면 서버에서 가져와야한다
        return this.service.getValue().loginForManager(branchId, pw)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> Observable.error(throwable))
                .flatMap(loginResponse -> {
                    LogUtil.d("로그인 중 flatMap : " + (Service.requestErrorHandler(loginResponse)));
                    if (Service.requestErrorHandler(loginResponse)) {
                        loginInfo.setValue(loginResponse.result);
                    } else {
                        return Observable.error(new NullPointerException("cannot login"));
                    }
                    return Observable.just(loginInfo);
                });
    }

    public Observable<ArrayList<TableMapInfoData>> getMapInfo(String branchUid) {

        ///1. 로컬에 저장된 로그인 정보가 없는지 확인할것.

        ///2. 로컬에 없다면 서버에서 가져와야한다
        return this.service.getValue().getMapInfo(branchUid)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> Observable.error(throwable))
                .flatMap(tableMapInfoData -> {
                    this.tableMapInfo.setValue(tableMapInfoData);
                    return Observable.just(tableMapInfo.getValue());
                });
    }

    public Observable<Integer> bufferSize;


    /**
     * 태블릿 호잇용
     **/
    public Observable initSetting() {

        this.subCategories_b.clear();
        this.subCategories.clear();
        this.mainCategories.clear();


        return this.service.getValue().getNoticeList(loginInfo.getValue().branchInfo.storeBranchUid)
                .subscribeOn(Schedulers.computation())
                .onErrorResumeNext(throwable -> Observable.error(throwable))
                .flatMap(noticeResponse -> {
                    if (Service.requestErrorHandler(noticeResponse)) {

                        if (noticeResponse != null) {
                            ///노티 저장.
                            try {
                                LoginUtil.getInstance(null).setmNotice(noticeResponse.result);
                            } catch (Exception e) {
                            }
                        }
                        return Observable.just(true);
                    } else {
                        if (noticeResponse.getStatus().code.equals("E2000")) {
                            return Observable.error(new LoginInfoNotFoundException("로그인 정보가 없음"));
                        }
                        return Observable.error(new Exception("에러코드"));
                    }
                })
                .flatMap(aBoolean -> this.service.getValue().getMainCategoryList())
                ////메인 카테고리 정보 가져옴.
                .subscribeOn(Schedulers.computation())
                .onErrorResumeNext(throwable -> Observable.error(throwable))
                .flatMap(orderCategoryResponse -> {
                    LogUtil.d("로그인 스트림 1 flatMap: ");
                    if (Service.requestErrorHandler(orderCategoryResponse)) {

                        //결과 처리. 최소 1개이상이란건 정상이란 이야기니까
                        if (orderCategoryResponse.result != null
                                && orderCategoryResponse.result.categories != null
                                && orderCategoryResponse.result.categories.size() > 0) {

                            for (OrderCategoryData.CategoryItem item : orderCategoryResponse.result.categories) {
                                subCategories_b.put(item.categoryCode, false);
                                mainCategories.add(item);    //메인카테고리 저장하는 맵
                            }

                            bufferSize = Observable.just(mainCategories.size());

                            LogUtil.d("카테고리의 갯수 : " + mainCategories.size());

                            return Observable.from(orderCategoryResponse.result.categories);
                        } else {
                            return Observable.error(new NullPointerException("empty main category"));
                        }
                    } else {
                        if (orderCategoryResponse.getStatus().code.equals("E2000")) {
                            return Observable.error(new LoginInfoNotFoundException("로그인 정보가 없음"));
                        }
                        return Observable.error(new Exception("에러코드"));
                    }
                })
                //// 서브 카테고리 정보를 요청함
                .subscribeOn(Schedulers.computation())
                .onErrorResumeNext(throwable -> Observable.error(throwable))
                .flatMap(categoryItem -> {
                    LogUtil.d("로그인 스트림 2 flatMap: ");
                    return this.service.getValue().getSubCategoryList(categoryItem.categoryCode);
                })
                .flatMap(orderCategoryResponse -> {


                    if (orderCategoryResponse != null && orderCategoryResponse.result.categories.size() > 0) {
                        LogUtil.d("onNext 카테고리 가져오기 observable size : " + orderCategoryResponse.result.categories.size());

                        subCategories.put(orderCategoryResponse.result.categories.get(0).parentCategory, orderCategoryResponse.result.categories);
//                        TabletSetting.getInstance()
//                                .setOrderSubCategoryList(orderCategoryResponse.result.categories.get(0).parentCategory, orderCategoryResponse.result.categories);
                    }
                    return Observable.just(true);
                })
                .toList()   ///각 카테고리 요청한걸 한데 모아서 처리한다
                .flatMap(booleans -> {
//                    LogUtil.d("toList 함수의 기능이 뭐지 : " +  booleans.size());
//                    for(Boolean tmp : booleans)
//                    {
//                        LogUtil.d("toList 함수의 기능이 뭐지 ---  : " +  tmp);
//                    }
//                    if(booleans.contains(false))
//                        return Observable.error()


                    return Observable.just(true);
                });

        ////공지사항 가져오기


    }


    /**
     * 코코이찌방야에서 사용하는 초기셋팅
     **/
    public Observable initSettingForCOCO(int tableMapWidthSpan) {

        this.subCategories_b.clear();
        this.subCategories.clear();
        this.mainCategories.clear();
        this.allMenus.clear();


        return this.service.getValue().getMapInfo(loginInfo.getValue().branchInfo.storeBranchUid)
                .subscribeOn(Schedulers.computation())
                .onErrorResumeNext(throwable -> Observable.error(throwable))
                .flatMap(mapList -> {

                    LogUtil.d("테이블 사이즈즈즈즈 : " + mapList.size());
                    TabletSetting.getInstance().mTableMapInfo.clear();
                    if (mapList != null) {
                        ///가져온 테이블 맵 정렬로직
                        ///1. 숫자가 앞으로 오도록
                        ///2. 가져온 숫자 테이블의 경우 SPANCOUNT 로 나눌때 나머지가 발생하면, 더미 테이블을 추가할것.
                        Collections.sort(mapList, new Comparator<TableMapInfoData>() {
                            @Override
                            public int compare(TableMapInfoData t, TableMapInfoData t1) {

                                ///정렬순서: 숫자/DABF 순서
                                ///1. 숫자 테이블 정렬
                                try {
                                    Integer.parseInt(t.tNo);
                                    Integer.parseInt(t1.tNo);

                                    return t.tNo.compareTo(t1.tNo);

                                } catch (Exception e) {


                                }

                                String type1 = t.tNo.substring(0, 1);
                                String type2 = t1.tNo.substring(0, 1);

                                if (type1.equals(type2)) ///서로 같은 타입
                                {
                                    return t.tNo.compareTo(t1.tNo);
                                } else//서로 다른 타입
                                {
                                    ///혹시라도 둘중 하나에 일반 테이블이 껴있는지 확인해야함
                                    Boolean isNumeric = false;

                                    try {
                                        Integer.parseInt(t.tNo);
                                        isNumeric = true;
                                    } catch (Exception e) {
                                    }

                                    try {
                                        Integer.parseInt(t1.tNo);
                                        isNumeric = true;
                                    } catch (Exception e) {
                                    }

                                    if (isNumeric)   ///둘중하나에 숫자가 존재함
                                    {
                                        return t.tNo.compareTo(t1.tNo);
                                    }


                                    ///1. D에 대한 처리
                                    if (type1.equals("D")) {
                                        return -1;
                                    }

                                    if (type2.equals("D")) {
                                        return 1;
                                    }
                                    ///2. A에 대한 처리
                                    if (type1.equals("A")) {
                                        return -1;
                                    }

                                    if (type2.equals("A")) {
                                        return 1;
                                    }

                                    ///3. B에 대한 처리
                                    if (type1.equals("B")) {
                                        return -1;
                                    }

                                    if (type2.equals("B")) {
                                        return 1;
                                    }

                                    ///4. F에 대한 처리
                                    if (type1.equals("F")) {
                                        return -1;
                                    }

                                    if (type2.equals("F")) {
                                        return 1;
                                    }

                                }


                                return 0;
                            }
                        });


                        int tNormal = 0;
                        int tD = 0;
                        int tA = 0;
                        int tB = 0;
                        int tF = 0;

                        int insertIndex_Normal = 0;
                        int insertIndex_D = 0;
                        int insertIndex_A = 0;
                        int insertIndex_B = 0;
                        int insertIndex_F = 0;

                        for (int i = 0; i < mapList.size(); i++) {
                            try {
                                Integer.parseInt(mapList.get(i).tNo);
                                tNormal++;
                                insertIndex_Normal = i;
                            } catch (Exception e) {
                                switch (mapList.get(i).tNo.substring(0, 1)) {
                                    case "D":
                                        tD++;
                                        insertIndex_D = i;
                                        break;
                                    case "A":
                                        tA++;
                                        insertIndex_A = i;
                                        break;
                                    case "B":
                                        tB++;
                                        insertIndex_B = i;
                                        break;
                                    case "F":
                                        tF++;
                                        insertIndex_F = i;
                                        break;
                                }
                            }
                        }
                        if (insertIndex_Normal != 0) insertIndex_Normal++;
                        if (insertIndex_D != 0) insertIndex_D++;
                        if (insertIndex_A != 0) insertIndex_A++;
                        if (insertIndex_B != 0) insertIndex_B++;
                        if (insertIndex_F != 0) insertIndex_F++;


                        LogUtil.d("총 테이블 : " + mapList.size());
                        LogUtil.d("일반 테이블 : " + tNormal);
                        LogUtil.d("D 테이블 : " + tD);
                        LogUtil.d("A 테이블 : " + tA);
                        LogUtil.d("B 테이블 : " + tB);
                        LogUtil.d("F 테이블 : " + tF);


                        ArrayList<TableMapInfoData> DUMMY_N = new ArrayList<>();
                        ArrayList<TableMapInfoData> DUMMY_D = new ArrayList<>();
                        ArrayList<TableMapInfoData> DUMMY_A = new ArrayList<>();
                        ArrayList<TableMapInfoData> DUMMY_B = new ArrayList<>();
                        ArrayList<TableMapInfoData> DUMMY_F = new ArrayList<>();
                        ///더비 테이블 추가 부분.
                        ///추가해야 할 데이터의 사이즈
                        int add_Normal = tableMapWidthSpan - tNormal % tableMapWidthSpan;
                        int add_D = tableMapWidthSpan - tD % tableMapWidthSpan;
                        int add_A = tableMapWidthSpan - tA % tableMapWidthSpan;
                        int add_B = tableMapWidthSpan - tB % tableMapWidthSpan;
                        int add_F = tableMapWidthSpan - tF % tableMapWidthSpan;

                        if (add_Normal == tableMapWidthSpan) add_Normal = 0;
                        if (add_D == tableMapWidthSpan) add_D = 0;
                        if (add_A == tableMapWidthSpan) add_A = 0;
                        if (add_B == tableMapWidthSpan) add_B = 0;
                        if (add_F == tableMapWidthSpan) add_F = 0;


                        ///추가할땐 역순으로 넣어줘야 index관리가 편함.
                        //1.F
                        if (add_F > 0) {
                            for (int i = 0; i < add_F; i++) {
                                DUMMY_F.add(new TableMapInfoData("ZDUMMY"));
                            }
                            mapList.addAll(insertIndex_F, DUMMY_F);
                        }
                        //2.B
                        if (add_B > 0) {
                            for (int i = 0; i < add_B; i++) {
                                DUMMY_B.add(new TableMapInfoData("ZDUMMY"));
                            }
                            mapList.addAll(insertIndex_B, DUMMY_B);
                        }
                        //3.A
                        if (add_A > 0) {
                            for (int i = 0; i < add_A; i++) {
                                DUMMY_A.add(new TableMapInfoData("ZDUMMY"));
                            }
                            mapList.addAll(insertIndex_A, DUMMY_A);
                        }
                        //4.D
                        if (add_D > 0) {
                            for (int i = 0; i < add_D; i++) {
                                DUMMY_D.add(new TableMapInfoData("ZDUMMY"));
                            }
                            mapList.addAll(insertIndex_D, DUMMY_D);
                        }
                        //5.Normal
                        if (add_Normal > 0) {
                            for (int i = 0; i < add_Normal; i++) {
                                DUMMY_N.add(new TableMapInfoData("ZDUMMY"));
                            }
                            mapList.addAll(insertIndex_Normal, DUMMY_N);
                        }


                        ///노티 저장.
                        TabletSetting.getInstance().mTableMapInfo.addAll(mapList);

                        LogUtil.d("테이블 저장 결과 : " + TabletSetting.getInstance().mTableMapInfo.size());

                    }
                    return Observable.just(true);
                })
                .flatMap(aBoolean -> this.service.getValue().getMainCategoryList())
                ////메인 카테고리 정보 가져옴.
                .subscribeOn(Schedulers.computation())
                .onErrorResumeNext(throwable -> Observable.error(throwable))
                .flatMap(orderCategoryResponse -> {
                    LogUtil.d("로그인 스트림 1 flatMap: ");

                    //결과 처리. 최소 1개이상이란건 정상이란 이야기니까
                    if (orderCategoryResponse.result != null
                            && orderCategoryResponse.result.categories != null
                            && orderCategoryResponse.result.categories.size() > 0) {

                        for (OrderCategoryData.CategoryItem item : orderCategoryResponse.result.categories) {
                            subCategories_b.put(item.categoryCode, false);
                            mainCategories.add(item);    //메인카테고리 저장하는 맵
                        }
                        return Observable.from(orderCategoryResponse.result.categories);
                    } else {
                        return Observable.error(new NullPointerException("empty main category"));
                    }

                })
                //// 서브 카테고리 정보를 요청함
                .subscribeOn(Schedulers.computation())
                .onErrorResumeNext(throwable -> Observable.error(throwable))
                .flatMap(categoryItem -> {
                    LogUtil.d("로그인 스트림 2 flatMap: ");
                    return this.service.getValue().getSubCategoryList(categoryItem.categoryCode);
                })
                .flatMap(orderCategoryResponse -> {

                    if (orderCategoryResponse != null && orderCategoryResponse.result.categories.size() > 0) {
                        LogUtil.d("onNext 카테고리 가져오기 observable size : " + orderCategoryResponse.result.categories.size());

                        subCategories.put(orderCategoryResponse.result.categories.get(0).parentCategory, orderCategoryResponse.result.categories);
//                        TabletSetting.getInstance()
//                                .setOrderSubCategoryList(orderCategoryResponse.result.categories.get(0).parentCategory, orderCategoryResponse.result.categories);
                    }
                    return Observable.from(orderCategoryResponse.result.categories);//Observable.just(true);
                })
                ///가져온 서브 카테고리정보 이용해 메뉴 정보를 불러온다
                .flatMap(categoryItem -> this.service.getValue().getMenus(categoryItem.categoryCode, loginInfo.getValue().branchInfo.storeBranchUid))
                .flatMap(menuResponse -> {
                    LogUtil.d("불러온 메뉴 정보 : " + menuResponse.result.menus.size());
                    if (menuResponse != null && menuResponse.result.menus.size() > 0) {
                        LogUtil.d("불러온 메뉴 정보 tag: " + menuResponse.result.menus.get(0).tag);
                        this.allMenus.put(menuResponse.result.menus.get(0).tag, menuResponse.result.menus);
                    }
                    return Observable.just(true);
                })
                .serialize();


        ////공지사항 가져오기


    }


    public boolean checkSubCategoryList() {


//        boolean tmp = true;
//        for( String key : subCategories_b.keySet())
//        {
//            LogUtil.d("로그인 스트림 세부사항 key: " + key);
//            LogUtil.d("로그인 스트림 세부사항 value: " + subCategories_b.get(key).booleanValue());
//
//
//
//            if(!subCategories_b.get(key).booleanValue());
//            {
//                LogUtil.d("로그인 스트림 세부사항 당첨: " + subCategories_b.get(key).booleanValue());
//                tmp = false;
//            }
//        }

        LogUtil.d("로그인 스트림 sub: " + subCategories_b.containsValue(false));
        return !subCategories_b.containsValue(false);

    }

}
