package com.cdef.commonmodule.Repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.cdef.commonmodule.DataModel.MenuData;
import com.cdef.commonmodule.DataModel.MenuItemData;
import com.cdef.commonmodule.Exceptions.LoginInfoNotFoundException;
import com.cdef.commonmodule.Networking.Responses.OrderResponse;
import com.cdef.commonmodule.Networking.Service;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class OrderRepository extends BaseRepository {

    MutableLiveData<ArrayList<MenuItemData>> menuData = new MutableLiveData<>();
    public OrderRepository(Service service) {
        super(service);
    }

    public Observable<LiveData<ArrayList<MenuItemData>>> getMenuList(String categoryId, String StoreBranchUid) {

        ///1. 로컬에 저장된 로그인 정보가 없는지 확인할것.
        return this.service.getValue().getMenus(categoryId, StoreBranchUid)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> Observable.error(throwable))
                .flatMap(menuResponse -> {
                    if (Service.requestErrorHandler(menuResponse)) {
                        menuData.setValue(menuResponse.result.menus);
                    }
                    else
                    {
                        if(menuResponse.getStatus().code.equals("E2000"))
                        {
                            return Observable.error(new LoginInfoNotFoundException("로그인 정보가 없음"));
                        }
                        return Observable.error(new Exception("에러코드"));

                    }
                    return Observable.just(menuData);
                });

    }

    public Observable<OrderResponse> order(String url, String orderJson) {

        ///1. 로컬에 저장된 로그인 정보가 없는지 확인할것.
        return this.service.getValue().order(url, orderJson)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> Observable.error(throwable))
                .flatMap(orderResponse -> {
                    return Observable.just(orderResponse);
                });

    }



    /**
     * 메뉴중 특정한 플래그를 가진 것들을 불러올때 쓰는 api
     * */
    public Observable<LiveData<ArrayList<MenuItemData>>> getMenuFlagship(String categoryId, String StoreBranchUid) {

        ///1. 로컬에 저장된 로그인 정보가 없는지 확인할것.
        return this.service.getValue().getMenuFlagship(categoryId, StoreBranchUid)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> Observable.error(throwable))
                .flatMap(menuResponse -> {
                    menuData.setValue(menuResponse.result.menus);
                    return Observable.just(menuData);
                });

    }

}
