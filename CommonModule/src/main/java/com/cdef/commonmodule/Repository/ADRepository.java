package com.cdef.commonmodule.Repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.cdef.commonmodule.DataModel.ADActionData;
import com.cdef.commonmodule.DataModel.BannerDefaultData;
import com.cdef.commonmodule.DataModel.BannerItemData;
import com.cdef.commonmodule.Utils.LogUtil;
import com.cdef.commonmodule.Networking.Service;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class ADRepository extends BaseRepository {

    MutableLiveData<ArrayList<BannerDefaultData>> bannerList = new MutableLiveData<>();

    public ADRepository(Service service) {
        super(service);
    }

    public Observable<LiveData<ArrayList<BannerDefaultData>>> getBannerList(int members, String sex) {

        return this.service.getValue().getBannerList(members, sex)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> Observable.error(throwable))
                .flatMap(menuResponse -> {
                    LogUtil.d("ADRepository response status : " + menuResponse.getStatus());
                    LogUtil.d("ADRepository response banners size : " + menuResponse.result.banners.size());

                    bannerList.setValue(menuResponse.result.banners);
                    return Observable.just(bannerList);
                });

    }

    public Observable<BannerItemData> getRandomBanner(String tableNo, int members, String sex, String section) {

        return this.service.getValue().getRandomBanner(tableNo, members, sex, section)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> Observable.error(throwable))
                .flatMap(randomADResponse -> Observable.just(randomADResponse.result));
    }


    public Observable<BannerItemData> getBannerDetail(String uuid, String tableNo, int members, String sex) {
        return this.service.getValue().getBannerDetail(uuid, tableNo, members, sex)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> Observable.error(throwable))
                .flatMap(randomADResponse -> Observable.just(randomADResponse.result));
    }

    public Observable<ADActionData> getADAction(String uuid, String actionParam, String input, String tableNo, int members, String sex) {

        return this.service.getValue().getADAction(uuid, actionParam, input, tableNo, members, sex)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> Observable.error(throwable))
                .flatMap(ADActionResponse -> Observable.just(ADActionResponse.result));

    }

}
