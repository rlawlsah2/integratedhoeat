package com.cdef.commonmodule.Utils;

import com.cdef.commonmodule.DataModel.FirebaseDataSet.Order;

import java.util.Comparator;

/**
 * Created by kimjinmo on 2018. 3. 5..
 */

public class OrderSort implements Comparator<Order> {

    @Override
    public int compare(Order t0, Order t1) {

        try {
            return t1.time.compareTo(t0.time);
        }
        catch (Exception e)
        {
            return 0;
        }
    }
}



