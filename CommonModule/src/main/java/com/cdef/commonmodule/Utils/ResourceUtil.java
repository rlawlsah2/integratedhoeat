package com.cdef.commonmodule.Utils;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by kimjinmo on 2018. 3. 16..
 * <p>
 * 해당 지점 전용 리소스를 다운받아 관리하기위한 객체
 */

public class ResourceUtil {

    /**
     * 기본적으로 입력받아야 할 리소스
     * 1. 왼쪽상단 아이콘(인트로화면 가운데에 다시 쓰임) - 이미지
     * 2. 왼쪽상단 아이콘 밑에 테이블 번호를 표시할 원 - RGB
     * 3. 앱 뒷배경 - 이미지
     * 4. 인트로에 들어갈 배경 - 이미지
     * 5. 시작버튼 색상 - RGB
     * 6. 왼쪽 사이드 버튼영역 뒷 배경 색 - RGB
     * 7. 왼쪽 사이드 버튼 눌린 색상 & 가격표시 & 테이블 번호 색상 (세개 다 같은색상 사용) - RGB
     * 8. 세부 카테고리 버튼의 배경색상 - RGB
     * 9. 세부 카테고리 버튼 on/off 글씨 색상 - RGB
     **/

//    private String iMainIcon = "https://firebasestorage.googleapis.com/v0/b/psptablet-c8335.appspot.com/o/%E1%84%80%E1%85%A7%E1%86%BC%E1%84%89%E1%85%A5%E1%86%BC%E1%84%8C%E1%85%AE%E1%84%86%E1%85%A1%E1%86%A8.png?alt=media&token=f7d3a22c-3e8f-4968-98f5-4d504fd84cd6";
//    private String iMainIcon = "https://static1.squarespace.com/static/587eae02ff7c5019ea407ff2/t/598cec1ad2b85727598dd3f2/1502408585123/?format=500w"; //세로가 더 긴 케이
//    private String iMainIcon = "http://cfile28.uf.tistory.com/image/245E333E58EB03C61FCFE9"; //정사각형 300*300
//    private String iMainIcon = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAb1BMVEX///8Tj6wAjKoAh6bj8PQAhaVvssUkl7Jkqr9GoLj7/v/u9/kAiqkAhKR/ussAjarc6u+Sw9Gv091Vp703mrSnzNi62OHV6O7r9fcXkq5cqb/H4OiHvc3O5OoAgKLY6u+bydZ1tceoz9qOw9KCvMxvl1OMAAAMrUlEQVR4nO2d63qiMBCGSRAPFaRVa7W1re32/q9xITM5TAgKGE4+zI9dOZTmNSH5MplJg2CyySabbLLJJptssskmm2yyySabbGz2/dJ3Cdq22de/vovQss3C8KfvMrRrs5CFy74L0aplhCze9V2KNi0nZPGq72K0aIKQ8f1T3wVpzYCQ8dO675K0ZUjIOD/2XZSWTBJmL+Os77K0Y5qQRW99F6YVMwhZ9NF3adowk5Al276L04IRQpZc+i6Pf6OELHo8HW4RsnDTd4l8m03IwnnfRfJsBcKH0+FFwkfT4Q7CB9PhLsLH0uGaMOUaMX0gHa4I+fwlMl/Gh9HhmnAZEMSH0eEmYXBJTMQH0eGEMPg1EZND34XzYpQwOBDE375L58MswuCDNNRHcPnbhMEH6W4eQIcXCINPUwM8gA4vEgbPZFx877N0PsxBGMxiE3HsOtxFGBy5oeDGrsOdhMGapQbiuHW4mzBDNHV4OmbEEsLgaWE21DHr8DLCINgbiGPW4eWEwcpETEarw68QBitz1BitDr9GGOwI4kh1+FXCYGkquJHq8OuEwZwg/nVdOh92gzDYjF6H3yIMzmPX4TcJqX9qhDr8NiH1T/F9l6XzYRUIgy1BXIxsqlGFkPqnUjYuHV6JkPqnOB+VDq9GGHyaiCz67qp4HqwiYfBGIxo+OyqeB6tKGDxTxPHo8MqE1D81otCb6oTUPzWe0JsahNQ/xaJzB8XzYHUIg/XJrMWRhMDXIqT+KRaPIgS+HqHlnxqFDq9LSJ03Y9DhtQmp82YEOrw+IUUcvg5vQEj9U4PX4U0Igx+i4OLn9ornwRoRUv8Uiwatw5sRUv/UsHV4Q0Lqnxq0Dnet41+pkX/qnSPBRUPW4UXCY5yUO34vOuV0S6f9g9XhBcJ1Jst4WtZ3bOP4JJcSiX9quDrcJlzDJDApKe9HaFz7oIgD1eEWoVq/j5lz1RcSMjkul1L/1EBD4Cnh00nPcRPXStMautBoDnL0jfSow9ThhNCa/p0cYgVrjcfQ4T4T580gdTghJJO/vBqLOULqWrQTkpv6p/gAdbhJuCelFdW4sGW1Dl/goRjm18Q/NUAdbhDurBoUZ+2hfGncFK7yiH7qnxqeDtfZCJwUVGPsSWLCxvwa0ugS2P6pwelwZ0aJVY2vxv0X2pLDvBnTDmpooTe3CfPWqPuPrfWuctEb0S5qWDq8CmHWGlWZP4q5brmMW1GX/6VHItsqEWbVuHsqvz+XccR5MygdXpEwGxtgiF9HrouZjHsniAPS4VUJs3p5F9XoIsxlHA0uGpAOr06YKbW8kzyVXjyTWhyMDq9BKGqKxmSaFs7n5NJQXP61CLMX7u2njJDZFwaiw+sRZjVV0krdyEPQ4XUJa9kgtoRplXAQOrxlQhb2HgLfNmH/qaitE/auw9sn7FuHG4RhlJmhS2JyLI5IySN9JipcJDf2uiWMJjwFT09Pxvwvk9rZ8WdsHBHZHeX3P0XWR6f1uiWMJgyhS1CEqTh8l0pFHJk+jCg/IQnVxxLEHkPgjV0jQCqfpdNbZFcc0T8aQzrJ0XBy1yHsU4cb72EiBMgMC5oIxaV48XbD1VaLsEcdbhBymJiDzwVrVO7IJ/uKb2N7t/y4MmF/qajmaAEVBX0NvJW/+FZGSkLryVNNwt5SUQkhjM2wG6b4iBMJLnwSwputPVF1CfvS4SYhrh3lHSb0MxInEXBQxWr2VJuwpy1hiKaBTNH8XYN+ZgducHgnP76EL0oNmA0Ie0lFJYQchq095yK/aYZjA/jpdxy6Q/kiNiHsQ4dbuwqK/m4bQz+D4zs03gw3Eo1M+vUbEfYQekMJ44s4GTGzyOApPccs3pinmxF2n4pqzy3EybnoZ2RlgXw7LRYLuPrH7yHsXIdbhNA8P0W3Luv11foRlG5NCbvW4RZhqsWVHCrCws+AdGtM2LEOt1tppIYsVC+8uMjybWPVJOxWh9uEXDYhqsDX0sQ1AV8g/IrQKiB2qMOLu3viBfRt4xAZYuG/BKJowDahNvdyOf0iu9PhBUKcCaoxQTg8DyqeAXLyF1cJS/3+JmJnOrzoiVqI8zhU4CRKzyhw/hHeS9jdljBFQtCO2NLAoTvTrxbOPxRWw1bKutsSpkjIV3+bjVwoS8+bzebPXFFb/IkzeSebfQBhBx+lnSvVIesqFdXhL+W5kQPuvGxc4cQqAnYUl9KBR/gaYgc6vF/CLnR4z4QdbAnTN2H7WzP2Tti6Du+fsO0tYQZA2LIOJ4QLaid1zrhsFu2ENxYfUQ+x1RB48jdKLL2fSxmhUkF9pvojFkx+98rDGMohvJpw009atCdSCaH1axQhrBty/dEiVNhyAaea+DasxVRUL4RySUpNn2sTtqjD/RCiT0dL6dqE7enwIuF8KS3vaaoRItHCOq5nLW0JUyQ8kTnCbcLnvH8SMybhT56tmxK2pMOLhLSnv034nZdL7K4snFYvT40J29HhjjqM0dJqhLN3/F7A48HuIGQif6N1wt9XtGVajfAr//cSw/rG7OsewjZC4K/0pS+8ImFOtk7AD/lyH2ELOtwDYSKa6Q5WpnCBrjGhfx3ugTAUau/wlbvl3uDgDkLvOrxI+IRWnRA8jOLyht9N6FuHFwnTBD34FcfDWQidqJAkMbuf0HMI/P3jYfaERJYoU28eCP3q8NqEiVY8ijCWI3WmwH0Qek1FrU24X6FxTSgFaj6L8kKYjRreQuDrEmqLNCEuMoqZsCdCfzrcDyGu9efTLV+E3nS4H0Lo4Nd5CIM3Ql8h8EU/jYPQ8NMUCY9iQTiP3hBrjrDSVtNPU4J48U3IRQ9CE5tO2KvktiLG5Rn4JuRPcn3ubvOiw2lcm2Nt7OoamnE/vcsPoZdU1CF4hK+Yh1TUgRN6CL0ZOuH9OnzwhHfr8OET3puKOgLCO3U4IUwdMRWuc/B74zCz2DmSZJYy5gzb0Len5IjZl8l2R3focJMwfV8Sy0fwlJ5aSsUTxz/bt+fnj8sOZspL21Yp4zt8ykn40ff6N+3FLRJxkX0mm+NwURAT+Y4tYW6trlnhTnINJtS+2+MyNv350g4xC2f4FIgG1JFVoSiv/DOEsCRnisXwaJ+5I/Tm1sqMTQ2E9MX44Q7CbCYFc7zsKRg+rkOr4BDrEH52axQEvhoqkBvr8EaEdJnoELnq0CTEaYlcSAVgWaUhFt0Q6y7Cxjq8CSHGnAbfh7y1iZ333K1UEeLGKDIFR/yeH8mLP2DsXuQkZGGzEPgi4U7NHsQ5QahcF2L+AC/O8RTFYfSK++5BfK0o2T7B9FqjDsHNgXF+QmvKdWNswdkJI1/XSdhQhztmwGRkAMKEnhMdwVx857G5oYnqWvBQEWL4OASIQz+jElNUI9ExjSWEzXT4rTl+5NiUBgjpHq03CHH9G3IbxUe8h4u9C0UjXif0OY6AjiY63LG6Fpqra0CogtTF3bhPwiuLYjpQlxPigJH3NdAsZVw1JK5+Qavg5DmukBVe3+VfJNxKEyMyECpPv1iwVwkK3y/7iPwtgXJCdKlGMrdK5jrgX5IWnYj6I7blhCyurcNvrcxYfSmM0kaS3XFjNNYrhNjb/sTAJGFg3Fml0KHuUvM57rCj2jq8ESGLjOH32e7l3YTQv3xH8D82SPTWRTicKJVzhbB2KmozQhYutuqCzn6+RggpjcHilP+r0uLEuPMT8hiqeFGBsK4OLxLO0I5/sSI8v6DpPZTiZPWK38i7lCPXCHGcf73k/8qhQuCKp5/FVex/bhBie2hMyKIQTJQC+1JXlDqPExgDXlVY2zVCSEZZiyYms/wLWjOtRFhLhzcZD5naKBNiJ7aVCA2fuRwqYrs0+GXdJKyjw6sR6jqE4sabdxgZRb38ViMM1UIEbiQZO2K8Q/2ck6PdaMSLX8L3uTQxt8j/wMwljsIEVNSmYh3K0fo7Mp/9eUAT1/5x9Zwf/JU/zsqsnIpajVBbvkqB2mn2jNcsPVlGiFlieqiAtzhCDQXvlpBuIfXLLJ21WDUVtS5hdn9qxYPo3QRvEMLWDEp/gmA7yJ/GJLIcvxJhVR3egJDtSQF+7VlPKSE+7AWYcIDUm7vCzg35JgDVCCvq8Fura7afBtbSzkobzN5N78ORElJguZAam1efdTeNrTYbXUOqWsoIq4XeFCPZradY0e1wmUerl+3hsD1T5c10ALzrEM6o4719lavf4PiVTquSitrUI8x53jt4W0ZrahVSUcfg875mt3X42AlvbwkzfsJbW8I8AOGNLWEegfB66M1DEF4NgX8Mwms6/EEIr+jwmXZnj9uiMh1+XM4fxJaXK93NZJNNNtlkk0022WSTTTbZZJM9pP0HNmiyIUsARlwAAAAASUVORK5CYII="; //정사각형 900*900

    private int iMainIcon = 0;//R.drawable.icon_logo;
    private String cTalbeNoCircle = "";//"#FF5690";
    private String iMainBackground = "";//"https://c.pxhere.com/photos/db/ec/wall_structure_stones_stone_wall_background_texture_masonry_marble-906283.jpg!d";
    private String iIntroBackground = "http://postfiles4.naver.net/MjAxNzAxMDNfNDcg/MDAxNDgzNDE3Mzg3Mzgz.y6BawnC5TVfMvzBbBNVPAtY_nfhYPjGfWfMYuxZoqvMg.fU7QLaoYWrvm-AnU6DqEob8m2BCU541oZjwI944N6xwg.JPEG.mina2910/image_72087031483417327852.jpg?type=w773";
    private String cStartButton = "#33ee88";  //"#E74545";
    private String cSideBackground = "#281b2b";
    private String cButtonSelected = "#ffdc60"; ///사이드 버튼 선택
    private String cButtonSubSelected = "#ffffff"; ///사이드 서브 버튼 선택
    private String cSubCategoryButtonBackground = "#AB8212";
    private String cSubCategoryButtonTextSelected;

    private static final double SELECTED_EFFECT_COLOR_RATIO = 0.7;
    private static final String HEX_PATTERN = "^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{8}|[A-Fa-f0-9]{3})$";
    private Pattern pattern;
    private Matcher matcher;


    /**
     * 리소스가 입력이 된 후 몇몇 리소스는 drawable obj로 생성이 되어야 사용가능. 따라서 미리 필요한 리소스를 생성해놓는게 효율적일것.
     **/
    private GradientDrawable dGDStartButtonSelected, dGDStartButtonUnSelected = null;
    private StateListDrawable dStartButton = null;

    private GradientDrawable dGDSubCategoryButtonSelected, dGDSubCategoryButtonUnSelected = null;
    private StateListDrawable dSubCategoryButton = null;


    private GradientDrawable dTalbeNoCircle = null;


    public int getiMainIcon() {
        return iMainIcon;
    }

    public void setiMainIcon(int iMainIcon) {
        this.iMainIcon = iMainIcon;
    }

    public String getcTalbeNoCircle() {
        return cTalbeNoCircle;
    }

    public void setcTalbeNoCircle(String cTalbeNoCircle) {
        this.cTalbeNoCircle = cTalbeNoCircle;
    }

    public String getiMainBackground() {


        return iMainBackground;
    }

    public void setiMainBackground(String iMainBackground) {
        this.iMainBackground = iMainBackground;
    }

    public String getiIntroBackground() {
        return iIntroBackground;
    }

    public void setiIntroBackground(String iIntroBackground) {
        this.iIntroBackground = iIntroBackground;
    }

    /**
     * 시작 버튼 리소스 결과물
     **/
    public Drawable getStartButton(int height) {

        if (dGDStartButtonSelected == null) {
            dGDStartButtonSelected = new GradientDrawable();
            this.dGDStartButtonSelected.setColor(Color.parseColor(getSelectedColor(this.cStartButton)));
            this.dGDStartButtonSelected.setCornerRadius(height);
        }

        if (dGDStartButtonUnSelected == null) {
            dGDStartButtonUnSelected = new GradientDrawable();
            this.dGDStartButtonUnSelected.setColor(Color.parseColor(this.cStartButton));
            this.dGDStartButtonUnSelected.setCornerRadius(height);
        }

        if (dStartButton == null) {
            dStartButton = new StateListDrawable();
            dStartButton.addState(new int[]{android.R.attr.state_pressed}, this.dGDStartButtonSelected);
            dStartButton.addState(new int[]{}, this.dGDStartButtonUnSelected);
        }
        return dStartButton.getConstantState().newDrawable();
    }

    public String getcStartButton() {
        return cStartButton;
    }


    /**
     * 테이블 번호 배경 리소스
     **/
    public Drawable getTableNoBackground(int height) {

        ///빈값이면 디폴트 이미지를 보내자
        if (this.cTalbeNoCircle == null || !this.colorHexValidate(this.cTalbeNoCircle)) {
            ///기본 이미지 뿌려줘라
//            return OrderApplication.mContext.getResources().getDrawable(R.drawable.img_tableno);
            return null;
        }


        if (dTalbeNoCircle == null)
            dTalbeNoCircle = new GradientDrawable();

        dTalbeNoCircle.setShape(GradientDrawable.RECTANGLE);
        dTalbeNoCircle.setCornerRadius(height);
        dTalbeNoCircle.setColor(Color.TRANSPARENT);
        dTalbeNoCircle.setStroke(5, Color.parseColor(this.cTalbeNoCircle));

        return dTalbeNoCircle.getConstantState().newDrawable();
    }


    /**
     * 서브카테고리 버튼 리소스 결과물
     **/
    public Drawable getSubCategoryButtonBackground() {

        LogUtil.d("서브 카테고리 색 테스트 1 : " + getSelectedColor(this.cSubCategoryButtonBackground));
        LogUtil.d("서브 카테고리 색 테스트 2 : " + (this.cSubCategoryButtonBackground));


        if (dGDSubCategoryButtonSelected == null) {
            dGDSubCategoryButtonSelected = new GradientDrawable();
            this.dGDSubCategoryButtonSelected.setColor(Color.parseColor(getSelectedColor(this.cSubCategoryButtonBackground)));
            this.dGDSubCategoryButtonSelected.setCornerRadius(4);
        }

        if (dGDSubCategoryButtonUnSelected == null) {
            dGDSubCategoryButtonUnSelected = new GradientDrawable();
            this.dGDSubCategoryButtonUnSelected.setColor(Color.parseColor(this.cSubCategoryButtonBackground));
            this.dGDSubCategoryButtonUnSelected.setCornerRadius(4);
        }

        if (dSubCategoryButton == null) {
            dSubCategoryButton = new StateListDrawable();
            dSubCategoryButton.addState(new int[]{android.R.attr.state_selected}, this.dGDSubCategoryButtonSelected);
            dSubCategoryButton.addState(new int[]{-android.R.attr.state_selected}, this.dGDSubCategoryButtonUnSelected);
        }
        return dSubCategoryButton.getConstantState().newDrawable();
    }


    public String getSelectedColor(String origin) {
        String result = null;
        int a = 255;
        int r, g, b = 0;
        try {
            if (this.colorHexValidate(origin)) {
                ///알파값 없는경우
                if (origin.length() == 7) {
                    r = (int) (hexToDecimal(origin.substring(1, 3)) * SELECTED_EFFECT_COLOR_RATIO);
                    g = (int) (hexToDecimal(origin.substring(3, 5)) * SELECTED_EFFECT_COLOR_RATIO);
                    b = (int) (hexToDecimal(origin.substring(5, 7)) * SELECTED_EFFECT_COLOR_RATIO);
                    result =
                            "#" + decimalToHex(r)
                                    + decimalToHex(g)
                                    + decimalToHex(b);
                } else if (origin.length() == 9) {
                    a = hexToDecimal(origin.substring(1, 3));
                    r = (int) (hexToDecimal(origin.substring(3, 5)) * SELECTED_EFFECT_COLOR_RATIO);
                    g = (int) (hexToDecimal(origin.substring(5, 7)) * SELECTED_EFFECT_COLOR_RATIO);
                    b = (int) (hexToDecimal(origin.substring(7, 9)) * SELECTED_EFFECT_COLOR_RATIO);
                    result =
                            "#" + decimalToHex(a)
                                    + decimalToHex(r)
                                    + decimalToHex(g)
                                    + decimalToHex(b);
                }
            }
        } catch (Exception e) {
            result = null;

        } finally {

            LogUtil.d("눌린 색상 : " + result);
            return result;
        }

    }

    public void setcStartButton(String cStartButton) {
        this.cStartButton = cStartButton;
        ///startButton 초기화

    }

    public String getcSideBackground() {
        return cSideBackground;
    }

    public void setcSideBackground(String cSideBackground) {
        this.cSideBackground = cSideBackground;
    }

    ////사이드 버튼(주문하기 같은 메인)
    public String getcButtonSelected() {

        return cButtonSelected;
    }

    public void setcButtonSelected(String cButtonSelected) {
        this.cButtonSelected = cButtonSelected;
    }

    public String getcButtonSubSelected() {
        return cButtonSubSelected;
    }

    public void setcButtonSubSelected(String cButtonSubSelected) {
        this.cButtonSubSelected = cButtonSubSelected;
    }

    public String getcSubCategoryButtonBackground() {
        return cSubCategoryButtonBackground;
    }

    public void setcSubCategoryButtonBackground(String cSubCategoryButtonBackground) {
        this.cSubCategoryButtonBackground = cSubCategoryButtonBackground;
    }

    public String getcSubCategoryButtonTextSelected() {
        return cSubCategoryButtonTextSelected;
    }

    public void setcSubCategoryButtonTextSelected(String cSubCategoryButtonTextSelected) {
        this.cSubCategoryButtonTextSelected = cSubCategoryButtonTextSelected;
    }

    public String getcSubCategoryButtonTextUnSelected() {
        return cSubCategoryButtonTextUnSelected;
    }

    public void setcSubCategoryButtonTextUnSelected(String cSubCategoryButtonTextUnSelected) {
        this.cSubCategoryButtonTextUnSelected = cSubCategoryButtonTextUnSelected;
    }

    /**
     * selected/unselected 타입의 리소스를 만든다.
     */
//    public GradientDrawable getButtonResource() {
//        GradientDrawable gd = new GradientDrawable();
//
//
//    }

    private String cSubCategoryButtonTextUnSelected;


    private static ResourceUtil instance;

    private ResourceUtil() {
        pattern = Pattern.compile(HEX_PATTERN);

    }

    public static synchronized ResourceUtil getInstance() {
        if (instance == null)
            instance = new ResourceUtil();

        return ResourceUtil.instance;
    }


    /**
     * 입력된 hex값이 유효한지 체크함.
     */
    public boolean colorHexValidate(final String hex) {
        if (hex == null)
            return false;
        matcher = pattern.matcher(hex);
        return matcher.matches();

    }

    /**
     * 2자리 hex값을 입력한다 가정.
     */
    private int hexToDecimal(String hex) {
        return Integer.parseInt(hex, 16);
    }

    private String decimalToHex(int decimal) {
        ///일단 0보다 커야지
        if (decimal > 0) {
            if (decimal <= 9) //0~9
            {
                return "0" + decimal;
            } else if (decimal < 16)   // 10~15
            {
                return "0" + returnHex(decimal);
            } else if (decimal < 256)  //16~255
            {
                return returnHex(decimal / 16) + returnHex(decimal % 16);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private String returnHex(int number) {
        switch (number) {
            case 15:
                return "F";
            case 14:
                return "E";
            case 13:
                return "D";
            case 12:
                return "C";
            case 11:
                return "B";
            case 10:
                return "A";
            default:
                return "" + number;
        }
    }


    public Drawable resizedImage(Drawable inputImage, int viewWidth, int viewHeight) {
        try {
            Bitmap tmp = ((BitmapDrawable) inputImage).getBitmap();

            int width = tmp.getWidth();
            int height = tmp.getHeight();
            double wph = (width / (height * 1.0));
            double hpw = (height / (width * 1.0));

            LogUtil.d("이미지 리사이징 전  width : " + ((BitmapDrawable) inputImage).getBitmap().getWidth());
            LogUtil.d("이미지 리사이징 전  height : " + ((BitmapDrawable) inputImage).getBitmap().getHeight());
            if (width > height)  //가로가 더 긴경우
            {
                LogUtil.d("이미지 리사이징 전  (viewWidth * (hpw) : " + (viewWidth * (hpw)));
                inputImage = new BitmapDrawable((Bitmap.createBitmap(tmp, 0, 0, viewWidth, (int) (viewWidth * (hpw)))));
            } else {

                LogUtil.d("이미지 리사이징 전  (viewHeight * (wph)) : " + (viewHeight * (wph)));
                inputImage = new BitmapDrawable((Bitmap.createBitmap(tmp, 0, 0, viewHeight, (int) (viewHeight * (wph)))));
            }

            LogUtil.d("이미지 리사이징 후  width : " + ((BitmapDrawable) inputImage).getBitmap().getWidth());
            LogUtil.d("이미지 리사이징 후  height : " + ((BitmapDrawable) inputImage).getBitmap().getHeight());

            return inputImage;

        } catch (Exception e) {
            LogUtil.d("이미지 리사이징 도중 에러  error : " + e);
            LogUtil.d("이미지 리사이징 도중 에러  error : " + e.getMessage());

            return inputImage;
        }

    }

}
