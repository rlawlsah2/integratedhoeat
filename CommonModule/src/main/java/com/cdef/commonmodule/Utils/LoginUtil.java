package com.cdef.commonmodule.Utils;

import android.content.Context;
import android.databinding.ObservableField;

import com.cdef.commonmodule.DataModel.LoginData;
import com.cdef.commonmodule.DataModel.NoticeData;

/**
 * Created by kimjinmo on 2017. 9. 20..
 * 태블릿 로그인 정보를 저장
 */

public class LoginUtil {
    public LoginData.branchInfo branchInfo;
    public LoginData loginData;

    public String getBTModuleName()
    {
        return "DreamEn";
    }

    public String getsBranchId() {

        return sBranchId;
    }

    public String getsBrandchPw() {
        return sBrandchPw;
    }

    public String getsTableNo() {
        return sTableNo;
    }
    public String getsTableNoWithTno() {
        return

                sTableNo;
    }
    public String getsBranchID() {
        return sBranchId;
    }


    public String getmSelectedTableNo() {
        return mSelectedTableNo.get();
    }

    public void setmSelectedTableNo(String mSelectedTableNo) {

        this.preSelectedTableNo = this.mSelectedTableNo.get();
        this.mSelectedTableNo.set(mSelectedTableNo);
    }
    private String preSelectedTableNo;
    public ObservableField<String> mSelectedTableNo = new ObservableField<>();


    public String getsBranchUid() {
        return sBranchUid;
    }

    public String getsAccessToken() {
        return sAccessToken;
    }
    public void setsAccessToken(String token)
    {
        this.sAccessToken = token;
    }

    public String getsPosIp() {
//        return "10.110.86.2";
        return sPosIp;
    }

    ////필요한 로그인 정보를 static으로 가지고 있는다.
    private String sBranchId;
    private String sBrandchPw;
    private String sTableNo;
    private String sBranchUid;
    private String sAccessToken;
    private String sPosIp;

    public String getmOrderSTBL_CD() {
        return mOrderSTBL_CD;
    }

    public void setmOrderSTBL_CD(String mOrderSTBL_CD) {
        this.mOrderSTBL_CD = mOrderSTBL_CD;
    }

    private String mOrderSTBL_CD;

    public String getmOrderID() {
        return mOrderID;
    }

    public void setmOrderID(String mOrderID) {
        this.mOrderID = mOrderID;
    }

    private String mOrderID;

    public int getmOrderIFSC_REMARK() {
        return mOrderIFSC_REMARK;
    }

    public void setmOrderIFSC_REMARK(int mOrderIFSC_REMARK) {
        this.mOrderIFSC_REMARK = mOrderIFSC_REMARK;
    }

    private int mOrderIFSC_REMARK;

    private static LoginUtil instance;

    public String getmBrandName() {
        return mBrandName;
    }

    public void setmBrandName(String mBrandName) {
        this.mBrandName = mBrandName;
    }

    private String mBrandName = null;

    public NoticeData getmNotice() {
        return mNotice;
    }

    public void setmNotice(NoticeData mNotice) {
        this.mNotice = mNotice;
    }

    private NoticeData mNotice;

    private LoginUtil(Context context)
    {
        sBranchId = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_ID);
        sBranchUid = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_STOREBRANCHUID);
        sBrandchPw = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_PW);
        sTableNo = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_TABLENO);
        sAccessToken = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_ACCESSTOKEN);
        sPosIp = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_POSIP);
    }

    /**
     * 갱신용 메소드
     * 사실 다른건 다 로컬에 가지고있는거라 token만 갱신하면 됨
     * */
    public void updateLoginIfo(Context context, String accessToken)
    {
        sBranchId = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_ID);
        sBranchUid = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_STOREBRANCHUID);
        sBrandchPw = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_PW);
        sTableNo = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_TABLENO);
        sPosIp = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_POSIP);

        PreferenceUtils.addStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_ACCESSTOKEN, accessToken);
        this.sAccessToken = accessToken;
    }

    public void setLoginInfo(Context context, String brandName, String id, String pw, String tableNo, String branchUid, String accessToken, String posIp, LoginData.branchInfo branchInfo)
    {
        PreferenceUtils.addStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_ID, id);
        PreferenceUtils.addStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_PW, pw);
        PreferenceUtils.addStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_TABLENO, tableNo);
        PreferenceUtils.addStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_ACCESSTOKEN, accessToken);
        PreferenceUtils.addStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_STOREBRANCHUID, branchUid);
        PreferenceUtils.addStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_POSIP, posIp);

        sBranchId = id;
        sBrandchPw = pw;
        sTableNo = tableNo;
        sAccessToken = accessToken;
        sBranchUid = branchUid;
        sPosIp = posIp;
        this.mBrandName = brandName;
        this.branchInfo = branchInfo;
    }

    /**
     * 무결성 검사. 5가지 변수값에 이상이 없는지 확인한다. 이상있을경우 false
     * **/
    public boolean isVaild()
    {
        if((sBranchId != null && sBranchId.length() > 0) &&
                (sBrandchPw != null && sBrandchPw.length() > 0) &&
//                (sTableNo != null && sTableNo.length() > 0) &&
                (sAccessToken != null && sAccessToken.length() > 0) &&
                (sBranchUid != null && sBranchUid.length() > 0))
//                (sPosIp != null && sPosIp.length() > 0))
        {
            return true;
        }
        else
            return false;
    }

    public static LoginUtil getInstance(Context context)
    {
        if(context == null && instance != null)
            return instance;

        if(instance == null)
            instance = new LoginUtil(context);
        return instance;
    }


    public static LoginUtil getInstance()
    {
        return instance;
    }


    public void getLocalData(Context context)
    {
        sBranchId = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_ID);
        sBranchUid = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_STOREBRANCHUID);
        sBrandchPw = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_PW);
        sTableNo = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_TABLENO);
        sPosIp = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_POSIP);
    }


}
